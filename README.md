## Run instructions

Open the project with android studio and run the App module

## Android test instructions

Open developer settings and disable the following options

* Window scale
* Transitions scale
* duration scale

Then you can run the android tests normally
