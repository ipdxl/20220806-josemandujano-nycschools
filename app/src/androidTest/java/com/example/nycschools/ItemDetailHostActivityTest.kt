package com.example.nycschools

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.example.nycschools.model.School
import com.example.nycschools.model.SchoolDetail
import com.example.nycschools.model.Service
import com.example.nycschools.network.NetworkModule
import dagger.Module
import dagger.Provides
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.unmockkAll
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class ItemDetailHostActivityTest {

    @Inject
    lateinit var service: Service


    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp() {
        hiltRule.inject()
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun testMainActivity(): Unit = runBlocking {
        ActivityScenario.launch(ItemDetailHostActivity::class.java)

        //Scroll down
        onView(withId(R.id.item_list)).perform(scrollToPosition<SchoolsAdapter.ViewHolder>(2))

        val overview = "overview paragraph"
        val detail = SchoolDetail("", "", overview, "")
//        every { detail.overviewParagraph } returns overview
        coEvery { service.getSchoolDetail(any()) } returns listOf(detail)

        //click on the third item
        onView(withId(R.id.item_list))
            .perform(actionOnItemAtPosition<SchoolsAdapter.ViewHolder>(2, click()))

        //wait for details to be loaded
        //check navigation
        onView(withId(R.id.item_detail_scroll_view)).check(matches(isDisplayed()))
    }
}

@Module
@TestInstallIn(components = [SingletonComponent::class], replaces = [NetworkModule::class])
class FakeDataServiceModule {

    @Provides
    fun bind(): Service {
        val service = mockk<Service>(relaxed = true, relaxUnitFun = true)

        val schools = listOf(
            School("12", "", "", "", ""),
            School("123", "", "", "", ""),
            School("1234", "", "", "", "")
        )
        coEvery { service.getSchools(any()) } returns schools
        return service
    }
}
