package com.example.nycschools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschools.model.School
import com.example.nycschools.model.Service
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.unmockkAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

internal class ItemListViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @RelaxedMockK
    lateinit var service: Service

    private lateinit var viewModel: ItemListViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxed = true, relaxUnitFun = true)
        viewModel = ItemListViewModel(service)
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun testViewModel() = runBlocking {
        //test with empty data
        coEvery { service.getSchools(any()) } returns emptyList()

        viewModel.onRefresh()
        delay(100)
        var value = viewModel.schools.value
        assertEquals(0, value?.size)

        //test with dummy data
        val mockList = listOf(mockk<School>())
        coEvery { service.getSchools(any()) } returns mockList

        viewModel.onRefresh()
        delay(100)
        value = viewModel.schools.value

        assertEquals(mockList, value)

        //Throw error
        coEvery { service.getSchools(any()) } throws Exception("Test Exception")

        viewModel.onRefresh()
        delay(100)
        value = viewModel.schools.value
        assertEquals(0, value?.size)
    }
}