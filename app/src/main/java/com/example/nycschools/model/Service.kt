package com.example.nycschools.model

import retrofit2.http.GET
import retrofit2.http.Query

interface Service {


    @GET("resource/f9bf-2cp4.json")
    suspend fun getSchools(@Query("${"$"}limit") limit: Int): List<School>

    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchoolDetail(@Query("dbn") dbn: String): List<SchoolDetail>

}