package com.example.nycschools.model

import com.google.gson.annotations.SerializedName

data class School(
    @field:SerializedName("dbn")
    val dbn: String,

    @field:SerializedName("sat_writing_avg_score")
    val satWritingAvgScore: String,

    @field:SerializedName("sat_critical_reading_avg_score")
    val satCriticalReadingAvgScore: String,

    @field:SerializedName("sat_math_avg_score")
    val satMathAvgScore: String,

    @field:SerializedName("school_name")
    val schoolName: String,
)
