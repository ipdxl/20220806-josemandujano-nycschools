package com.example.nycschools.model

import com.google.gson.annotations.SerializedName

data class SchoolDetail(
    @field:SerializedName("academicopportunities1")
    val academicopportunities1: String,

    @field:SerializedName("admissionspriority11")
    val admissionspriority11: String,

    @field:SerializedName("overview_paragraph")
    val overviewParagraph: String,

    @field:SerializedName("school_name")
    val schoolName: String
)
