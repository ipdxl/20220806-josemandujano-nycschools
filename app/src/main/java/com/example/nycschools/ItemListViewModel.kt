package com.example.nycschools

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.model.School
import com.example.nycschools.model.Service
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ItemListViewModel @Inject constructor(private val service: Service) : ViewModel() {

    private val _schools = MutableLiveData<List<School>>()
    val schools: LiveData<List<School>> = _schools

    init {
        onRefresh()
    }

    fun onRefresh() {
        viewModelScope.launch(Dispatchers.IO) {
            runCatching {
                service.getSchools(20)
            }.onFailure {
                _schools.postValue(emptyList())
                Log.e("ItemListViewModel", "onRefresh", it)
            }.onSuccess {
                _schools.postValue(it)
            }
        }

    }
}