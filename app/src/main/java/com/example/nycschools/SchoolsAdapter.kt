package com.example.nycschools

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.databinding.ItemListContentBinding
import com.example.nycschools.details.ItemDetailFragment
import com.example.nycschools.model.School

class SchoolsAdapter(
    private val itemDetailFragmentContainer: View?
) : RecyclerView.Adapter<SchoolsAdapter.ViewHolder>() {

    var values: List<School> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding =
            ItemListContentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.binding.item = item

        with(holder.itemView) {
            tag = item
            setOnClickListener { itemView ->
                val bundle = Bundle()
                bundle.putString(
                    ItemDetailFragment.ARG_ITEM_ID,
                    item.dbn
                )
                if (itemDetailFragmentContainer != null) {
                    itemDetailFragmentContainer.findNavController()
                        .navigate(R.id.fragment_item_detail, bundle)
                } else {
                    itemView.findNavController().navigate(R.id.show_item_detail, bundle)
                }
            }
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(val binding: ItemListContentBinding) :
        RecyclerView.ViewHolder(binding.root) {
    }

}