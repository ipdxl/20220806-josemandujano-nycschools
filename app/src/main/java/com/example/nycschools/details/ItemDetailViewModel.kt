package com.example.nycschools.details

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.model.SchoolDetail
import com.example.nycschools.model.Service
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ItemDetailViewModel @Inject constructor(private val service: Service) : ViewModel() {


    private val _details = MutableLiveData<SchoolDetail>()
    val details: LiveData<SchoolDetail> = _details

    var dbn: String? = null
        set(value) {
            field = value
            value?.let { fetchDetails(it) }
        }

    private fun fetchDetails(dbn: String) {
        viewModelScope.launch(Dispatchers.IO) {
            runCatching {
                service.getSchoolDetail(dbn).first()
            }.onFailure {
                Log.e("ItemDetailViewModel", "fetchDetails", it)
            }.onSuccess {
                _details.postValue(it)
            }
        }
    }
}